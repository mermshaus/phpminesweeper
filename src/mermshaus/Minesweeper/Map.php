<?php

namespace mermshaus\Minesweeper;

final class Map
{
    private $name;

    private $mines;

    private $sizeX;

    private $sizeY;

    private $rows = array();

    /**
     *
     * @param string $data
     */
    public function __construct($data)
    {
        $data = str_replace(array("\r\n", "\r"), "\n", $data);

        $jsonEnd = strpos($data, '}');

        $json = json_decode(substr($data, 0, $jsonEnd + 1), true);

        $map = rtrim(substr($data, $jsonEnd + 2));

        $rows = explode("\n", $map);

        $maxX = 0;
        $maxY = count($rows);

        foreach ($rows as $row) {
            $row = rtrim($row);

            $maxX = (strlen($row) > $maxX) ? strlen($row) : $maxX;
            $this->rows[] = $row;
        }

        $this->name = $json['name'];
        $this->mines = $json['mines'];
        $this->sizeX = $maxX;
        $this->sizeY = $maxY;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getMines()
    {
        return $this->mines;
    }

    public function getSizeX()
    {
        return $this->sizeX;
    }

    public function getSizeY()
    {
        return $this->sizeY;
    }

    public function getRows()
    {
        return $this->rows;
    }
}
