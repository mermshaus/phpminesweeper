<?php

namespace mermshaus\Minesweeper;

/**
 *
 * @author  Marc Ermshaus <marc@ermshaus.org>
 * @license GNU General Public License (Version 3 or later)
 *          <http://www.gnu.org/licenses/gpl.html>
 */
final class Game
{
    const STATE_RUNNING = 1;
    const STATE_WON     = 2;
    const STATE_LOST    = 3;

    /**
     *
     * @var array
     */
    private $field = array();

    /**
     *
     * @var int
     */
    private $state = self::STATE_RUNNING;

    /**
     *
     * @var int
     */
    private $numberOfMines = 0;

    /**
     *
     * @param string $file
     */
    public function  __construct($file)
    {
        $this->buildField($file);
    }

    /**
     *
     * @param Cell $cell
     */
    public function revealCell(Cell $cell)
    {
        // Only react if game is in running state
        if ($this->state !== self::STATE_RUNNING) {
            return;
        }

        if ($cell->isRevealed) {
            return;
        }

        if ($cell->isMine) {
            $this->state = self::STATE_LOST;
            $this->revealAll();
            return;
        }

        $cell->isRevealed = true;

        if (0 === $cell->adjacentMines) {
            foreach ($this->getAdjacentCells($cell) as $neighbor) {
                $this->revealCell($neighbor);
            }
        }

        $sizeX = $this->getSizeX();
        $sizeY = $this->getSizeY();

        // Count hidden fields
        $hidden = 0;
        for ($x = 0; $x < $sizeX; $x++) {
            for ($y = 0; $y < $sizeY; $y++) {
                if ($this->field[$x][$y] instanceof Cell && !$this->field[$x][$y]->isRevealed) {
                    $hidden++;
                }
            }
        }

        // If number of hidden fields equals number of mines, consider game won
        if ($hidden === $this->numberOfMines) {
            $this->state = self::STATE_WON;
            $this->revealAll();
        }
    }

    /**
     *
     * @param int $x
     * @param int $y
     * @return Cell
     */
    public function getCell($x, $y)
    {
        return $this->field[$x][$y];
    }

    /**
     *
     * @return int
     */
    public function getSizeX()
    {
        return count($this->field);
    }

    /**
     *
     * @return int
     */
    public function getSizeY()
    {
        return count($this->field[0]);
    }

    /**
     *
     * @return int
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     *
     */
    private function buildField($file)
    {
        $map = new Map(file_get_contents($file));

        $field = array_fill(0, $map->getSizeX(), array_fill(0, $map->getSizeY(), null));

        $y = 0;

        $cells = array();

        foreach ($map->getRows() as $row) {
            $x = 0;
            foreach (str_split($row) as $char) {
                if ('.' === $char) {
                    $c = new Cell();
                    $c->posX = $x;
                    $c->posY = $y;

                    $cells[] = $c;
                    $field[$x][$y] = $c;
                }
                $x++;
            }
            $y++;
        }

        shuffle($cells);

        for ($i = 0; $i < $map->getMines(); $i++) {
            $cells[$i]->isMine = true;
            $this->numberOfMines++;
        }

        $this->field = $field;

        // Count adjacent mines
        foreach ($cells as $cell) {
            $cell->adjacentMines = $this->countAdjacentMines($cell);
        }
    }

    /**
     *
     * @param Cell $cell
     * @return int
     */
    private function countAdjacentMines(Cell $cell)
    {
        return count(array_filter(
            $this->getAdjacentCells($cell),
            function (Cell $cell) {
                return $cell->isMine;
            }
        ));
    }

    /**
     *
     */
    private function revealAll()
    {
        $sizeX = $this->getSizeX();
        $sizeY = $this->getSizeY();

        for ($x = 0; $x < $sizeX; $x++) {
            for ($y = 0; $y < $sizeY; $y++) {
                if ($this->field[$x][$y] instanceof Cell) {
                    $this->field[$x][$y]->isRevealed = true;
                }
            }
        }
    }

    /**
     *
     * @param Cell $cell
     * @return array
     */
    private function getAdjacentCells(Cell $cell)
    {
        $cells = array();

        $offsets = array(
            array(-1, -1), array(-1, 0), array(-1, 1),
            array(0, -1), array(0, 1),
            array(1, -1), array(1, 0), array(1, 1)
        );

        foreach ($offsets as $offset) {
            $x = $cell->posX + $offset[0];
            $y = $cell->posY + $offset[1];

            if (!isset($this->field[$x][$y])) {
                continue;
            }

            if ($this->field[$x][$y] instanceof Cell) {
                $cells[] = $this->field[$x][$y];
            }
        }

        return $cells;
    }
}
