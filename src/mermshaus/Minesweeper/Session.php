<?php

namespace mermshaus\Minesweeper;

final class Session
{
    private $namespace;

    public function __construct($namespace)
    {
        $this->namespace = $namespace;
    }

    public function hasKey($key)
    {
        return (isset($_SESSION[$this->namespace][$key]));
    }

    public function getValue($key, $defaultValue = null)
    {
        return ($this->hasKey($key))
                ? $_SESSION[$this->namespace][$key]
                : $defaultValue;
    }

    public function setValue($key, $value)
    {
        $_SESSION[$this->namespace][$key] = $value;
    }

    public function deleteValue($key)
    {
        unset($_SESSION[$this->namespace][$key]);
    }

    public function clearData()
    {
        $_SESSION[$this->namespace] = array();
    }
}
