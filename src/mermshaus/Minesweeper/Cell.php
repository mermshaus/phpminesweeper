<?php

namespace mermshaus\Minesweeper;

/**
 *
 * @author  Marc Ermshaus <marc@ermshaus.org>
 * @license GNU General Public License (Version 3 or later)
 *          <http://www.gnu.org/licenses/gpl.html>
 */
final class Cell
{
    /**
     *
     * @var bool
     */
    public $isMine = false;

    /**
     *
     * @var bool
     */
    public $isRevealed = false;

    /**
     *
     * @var int
     */
    public $posX = -1;

    /**
     *
     * @var int
     */
    public $posY = -1;

    /**
     *
     * @var int
     */
    public $adjacentMines = 0;
}
