<?php

namespace mermshaus\Minesweeper;


// http://stackoverflow.com/a/8891890/90237
function url_origin( $s, $use_forwarded_host = false )
{
    $ssl      = ( ! empty( $s['HTTPS'] ) && $s['HTTPS'] == 'on' );
    $sp       = strtolower( $s['SERVER_PROTOCOL'] );
    $protocol = substr( $sp, 0, strpos( $sp, '/' ) ) . ( ( $ssl ) ? 's' : '' );
    $port     = $s['SERVER_PORT'];
    $port     = ( ( ! $ssl && $port=='80' ) || ( $ssl && $port=='443' ) ) ? '' : ':'.$port;
    $host     = ( $use_forwarded_host && isset( $s['HTTP_X_FORWARDED_HOST'] ) ) ? $s['HTTP_X_FORWARDED_HOST'] : ( isset( $s['HTTP_HOST'] ) ? $s['HTTP_HOST'] : null );
    $host     = isset( $host ) ? $host : $s['SERVER_NAME'] . $port;
    return $protocol . '://' . $host;
}

// http://stackoverflow.com/a/8891890/90237
function full_url( $s, $use_forwarded_host = false )
{
    return url_origin( $s, $use_forwarded_host ) . $s['REQUEST_URI'];
}

/**
 *
 * @author  Marc Ermshaus <marc@ermshaus.org>
 * @license GNU General Public License (Version 3 or later)
 *          <http://www.gnu.org/licenses/gpl.html>
 */

error_reporting(-1);
ini_set('xdebug.max_nesting_level', 10000);

/*
 * Autoloading
 *
 * (Note: Anonymous functions need PHP 5.3 to work. But this can be replaced
 * easily with a create_function() call.
 */

spl_autoload_register(function ($class) {
    $path = dirname(__DIR__) . '/src/' . str_replace('\\', '/', $class) . '.php';

    require $path;
});

/* Start */

session_start();
$session = new Session('minesweeper');
header('content-type: text/html; charset=utf-8');

$maps = array();

foreach (glob(dirname(__DIR__) . '/data/maps/*.txt') as $file) {
    $maps[substr(basename($file), 0, -4)] = new Map(file_get_contents($file));
}

// Make sure nobody sneaks in an invalid map name
if ($session->hasKey('map')) {
    if (!array_key_exists($session->getValue('map'), $maps)) {
        $session->deleteValue('map');
    }
}

if (!$session->hasKey('map')) {
    $tmp = array_keys($maps);
    shuffle($tmp);
    $session->setValue('map', $tmp[0]);
}

$mapPath = dirname(__DIR__) . '/data/maps/' . $session->getValue('map') . '.txt';

// Initialize session data
if (!$session->hasKey('game')) {
    /* Start new game */
    $game = new Game($mapPath);
} else {
    $game = $session->getValue('game');
}

// Perform form actions
if (isset($_POST['form_id'])) {
    if ('cell_clicked' === $_POST['form_id']) {
        // If anybody knows how do to this in one line, TELL ME
        $tmp = array_keys($_POST['cell']);
        $x   = array_pop($tmp);

        $tmp = array_keys($_POST['cell'][$x]);
        $y   = array_pop($tmp);

        $game->revealCell($game->getCell($x, $y));
    } elseif ('new_game' === $_POST['form_id']) {
        $session->clearData();
        $session->setValue('map', $_POST['map']);
        header('Location: ' . full_url($_SERVER));
        exit;
    }
}

$session->setValue('game', $game);

$e = function ($s) {
    return htmlspecialchars($s, ENT_QUOTES, 'UTF-8');
};

/* Everything from here on is just output */

?><!DOCTYPE html>

<html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Minesweeper</title>
        <link rel="stylesheet" href="assets/screen.css">
    </head>

    <body>
        <div class="minesweeper cf">
            <form method="post" action="<?=$e(full_url($_SERVER))?>">

                <?php for ($y = 0; $y < $game->getSizeY(); $y++): ?>

                <div class="row">
                    <?php for ($x = 0; $x < $game->getSizeX(); $x++): ?>

                    <div class="field">

                    <?php $cell = $game->getCell($x, $y); ?>

                    <?php if (null !== $cell) : ?>

                    <button class="cell<?php echo ($cell->isRevealed) ? ' revealed' : ''; ?>"
                           type="submit"
                           <?php echo ($cell->isRevealed || $game->getState() != Game::STATE_RUNNING)
                                   ? 'disabled="disabled"'
                                   : ''; ?>
                           name="cell[<?php echo $cell->posX; ?>][<?php echo $cell->posY; ?>]"
                           ><?php

                    if ($cell->isRevealed) {
                        if ($cell->isMine) {
                            echo '*';
                        } else {
                            echo ($cell->adjacentMines > 0) ? $cell->adjacentMines : '';
                        }
                    }

                    ?></button>

                    <?php endif; ?>

                    </div>

                    <?php endfor; ?>
                </div>

                <?php endfor; ?>

                <input type="hidden" name="form_id" value="cell_clicked" />
            </form>
        </div>

        <form method="post" action="<?=$e(full_url($_SERVER))?>">
            <p>
                <input type="hidden" name="form_id" value="new_game" />
                <input type="submit" value="Start new game" />

                <select name="map">
                <?php foreach ($maps as $file => $map) : ?>
                    <?php $selected = ($file === $session->getValue('map')) ? ' selected="selected"' : ''; ?>

                    <option value="<?=$e($file)?>"<?=$selected?>><?=$e($map->getName());?></option>
                <?php endforeach; ?>
                </select>
            </p>
        </form>

        <?php

        if ($game->getState() == Game::STATE_LOST) {
            echo '<div class="result loss">You lost! :-(</div>';
        } else if ($game->getState() == Game::STATE_WON) {
            echo '<div class="result win">You won! :-)</div>';
        }

        ?>

    </body>

</html>
